# CS2H01-Examen Final

Original Github link: [https://github.com/utec-computer-science/cs2h01-final-andrea-velasquez](https://github.com/utec-computer-science/cs2h01-final-andrea-velasquez)

1. Evaluar la interfaz de `terminal bash`   y determinar si es una interface usable,  argumentar su respuesta usando heurísticas (4)
* Visibilidad del sistema: Permite ver en que path del sistema estás y, a veces, si estás en una carpeta con .git, el branch en el que estás, cuando fue tu último commit o si tienes cambios que commitear.
* Eficiencia: Esta heuristica adquiere más importancia aquí pues el terminal está hecho para usuarios experimentados. Los comandos tienen shorcuts para los flags (-t en lugar de --tag por ejemplo), puedes concatenar comandos (&&) o crear scripts para (.bash, .sh) ejecutar tareas automatizadas.
* Ayuda y documentación: Puedes ingresar a la documentación completa de los comandos con un "man comando" e incluso puedes usar comando --help para una descripción más pequeña de cómo usarlo.
* Prevención de errores: A la mayoría comandos tienes que agregarles los flags correspondientes para acciones destructivas, como el flag --force. Ej.: rm no puede eliminar directorios, a menos que agregues -r)

Tomando en cuenta las heuristicas mencionadas, la interfaz es usable.


---
2. Explicar qué es un error de  Slip y  Lapse.  Explicar la diferencia entre cada uno  y qué se puede hacer evitarlos. (2)

* Slip y lapse son errores humanos que pueden darse al ejecutar un procedimiento.
* Un slip es un error en cómo se ejecuta el procedimiento. Aquí entran las fallas como tener la intención de hacer una cosa pero terminar haciendo otra (captura), aplicando pasos correctos pero con objetos distintos a los indicados (descripción) o realizar los pasos adecuados pero en orden incorrecto. Por ejemplo, intentar abrir un archivo en tu computadora, pero ir a la carpeta equivocada.
* Un lapse se relaciona a errores en la memoria. Como olvidar la intención (lo que se quería hacer) y abandonar el procedimiento antes de completarlo u omitir un paso por olvido o por interrupciones.

Para evitar...
* Slips: 
    * Opciones destructivas o peligrosas deben estar alejadas de las opciones más comunes.
    * Las opciones distintas no deben tener una descripción muy similar.
    * Los procedimientos para realizar acciones usuales deberían tener prefijos (primeros pasos) distintos.
* Lapse:
    * Evitar posibles interrupciones durante el procedimiento.
    * Obligar al usuario a realizar acciones brindando el resultado al final del procedimiento (ej.: devolver tarjeta en máquina expendedora antes de entregar el producto).
    * Evitar procedimientos muy largos.

---
3. Dado los siguientes videos: 

```
- https://www.youtube.com/watch?v=AttXbcLUyR0
- https://www.youtube.com/watch?v=dhGXhhCGZaY
- https://www.youtube.com/watch?v=X7T67Gdpp1c
```

Describa cómo la realidad aumentada puede ser diseñada para cambiar las actitudes y comportamiento de las personas?  Son interfaces usables? Argumente su respuesta   (3)

---
4. Defina que es un escenario, happy path  y storyboard. De ejemplos. (3)
* Para evaluar un sistema realizas storyboards, historias de un usuario ejecutando pasos para realizar un procedimiento. Ejemplo: Una serie de ilustraciones simples mostrando como comprar una entrada en una página web de un cine.
* Los escenarios son más complejos, dan un contexto de quién realiza el procedimiento, en qué situación o dónde. Ejemplo: Una secuencia de pasos que toma un jóven de unos 20 años, con poco tiempo disponible, para comprar unas entradas online de cine. Está en su celular.
* Los storyboards y escenarios muestran diferentes caminos tomados por los usuarios. El "happy path" vendría a ser el camino correcto, sin errores, que tomaría el usuario. Ejemplo: Al comprar entradas para un cine online, el happy path sería encontrar entradas disponibles en el cine que el usuario quería a la hora que el usuario quería. Se ignoran los otros caminos donde el usuario pierde el interés en la compra o no encuentra entradas, cine u horario deseado.


---
5. A que nos referimos con libertad y control de usuario? de un ejemplo.  (2)
El usuario puede seleccionar opciones por error o entrar en un estado indeseado. Darle libertad y control implica que el sistema proporcione formas de deshacer este error, una salida de ese estado indeseado. Por ejemplo, si el usuario elimina un archivo por error, el sistema debería poder darle la opción de recuperar el archivo. Un ejemplo común en páginas web es el botón de "Retroceder" o "Volver a ____" si es que el usuario entró a una sección equivocada y quiere regresar.

---
6. Caso de estudio: (6 pts)

Data la siguiente pagina:  https://votoinformado.jne.gob.pe , evaluar según las heurísticas de Nielsen.

* Visibilidad del estado del sistema: El sistema siempre debe mantener a los usuarios informados sobre lo que ocurre 
    * No hay forma de saber en que opción del menú de navegación estás.
    * Una vez entras a una subsección dentro de una subsección no sabes en que parte de la jerarquía de las secciones del website estás, tienes que retroceder hasta encontrar la sección principal.

* Real Mapping: Iconos y formas que el usuario asocie con facilidad
  * En debates electorales, aparece un ícono de play (triángulo) cuando haces hover, relacionándolo con iniciar video.
  * El ícono de una casita (home) sirve para salir de la sesión (la "sesión" vendría a ser la q). Uno esperaría que te lleve al "home" de tu cuenta: la página principal de procesos (la que recibes luego de iniciar sesión).
  * Cuando la navegación está cerrada aparece una opción con un ícono de engranaje, pero esta no envia a settings contrario a lo que se esperaría. El ícono es incorrectamente usado como indicador de que la barra lateral es un menú de navegación.

* Control y libertad del usuario:
    * Luego de entrar a cualquier página al hacer click en alguna opción no es posible regresar. La única opción es scrollear para que aparezcan las flechas de arriba y a la izquierda e incluso esto no es posible en las páginas cortas donde no puedes scrollear.

* Consistencia:
    * Se respeta la paleta de colores rojos y grises. Sin embargo, esto no aplica en los íconos coloridos de redes sociales flotantes de la izquierda en contraste con los que aparecen al costado de HOLA ${NOMBRE}
    * Los bloques clickeables tienen un efecto similar al pasar el mouse: Adquieren sombra. Pero a veces opciones que no deberían ser clickeables también tienen el efecto, como los procedimientos inactivos en procedimientos.
    * Hay distintas formas de comparar. Puedes comparar hojas de vida entre integrantes de una organización política tras encontrar organizaciones en búsqueda personalizada, pero también puedes ir a la opción de comparar candidatos. Ambas opciones muestran resultados diferentes.
    * Todos los menús 

* Prevención de errores del usuario:
    * Todo está basado en recuperar errores, no hay prevención. 

* Fácil de recordar:
    * Las opciones en el menú de navegación son entendibles y fáciles de recordar pero la gran cantidad de ventanas que se pueden abrir para realizar un procedimiento pueden provocar lapses.
    * No hay ningún historial o autocompletado con búsquedas pasadas.

* Eficiencia:
    * No existen accesos directos ni opciones rápidas para realizar acciones comunes o específicas.
    * No hay ningún mapa de sitio para acceder rápidamente a subsecciones.

* Minimalista:
    * No existe minimalismo, casi todos los íconos tienen que estar explicados, como "ver más información" 

* Diagnóstico y recuperación de errores:
    * Se muestra un "___ no encontrado" al no encontrar algo en cualquier búsqueda.
    * A veces el botón para cambiar de idioma funciona y otras no. Cuando lo hace y se selecciona un idioma distinto, el idioma no cambia. No se aprecia ningún feedback sobre por qué.
    * En Comparar Candidatos, te aparece un popup con un aviso sobre por qué tu comparación no es posible. Ej.: Solo has seleccionado un candidato.

* Ayuda y Documentación:
    * Es inexistente, no se muestra ninguna ventana de ayuda para explicar la estructura de la página web, las acciones que pueden realizarse o algún FAQ con explicaciones a, por ejemplo, por qué algunos procedimientos estan desactivados al entrar con un DNI, pero no cuando ingresas sin DNI.

	  
