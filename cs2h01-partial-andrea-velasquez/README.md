# Partial

Original Github link: [https://github.com/utec-computer-science/cs2h01-partial-andrea-velasquez](https://github.com/utec-computer-science/cs2h01-partial-andrea-velasquez)

1. A que nos referimos con un Trade-off de usabilidad? (1pts)

- - - -
2. Cuales son las Metas de usabilidad, Explicar. (3pts)

- - - -
3. Cuales son los principios de usabilidad (learnability), Explicar (3 pts)

- - - -
4. Que tipo de interaccion se logra a travez de los dispositivos como Alexa, Google Home o Siri Home Pod?  (1pts)

- - - -
6. Que tipo de interaccion se logra a travez de la realidad aumentada?  (1pts)

- - - -
7. Explique los criterios de una buena encuesta. De tres ejemplos de como realizar una buena pregunta.  (3pts)

- - - -
8. Cuando decimos que un feedback es innecesario de algun ejemplo   (2 pts)

- - - -
9. Caso de estudio, realizar: (6 pts)
	* Definicion de usuario
	* Analisis de tareas
	* Planes
	* Proponer una interfaz. 

```
Se requiere proponer una interfaz para el siguiente caso: Un usuario desea comprar entradas a travez de un cajero, cuando el usuario opera con uno de estos cajeros y selecciona la opción de entradas de cine, el cajero le pregunta la fecha deseada, entonces el cajero el cual se haya conectado con el ordenador central a través de una línea telefónica, obtiene la lista de películas para ese día en cada centro, las proyecciones programadas y el número de entradas disponibles para cada proyección. El usuario selecciona la película, la hora de proyección y en centro y el número de entradas. El cajero solicita el número de entradas indicado al ordenador central, el cual las marca como en venta para ese cajero. En ese momento el cajero le pide al usuario que introduzca su tarjeta de crédito, tras lo cual le pide el número secreto, una vez hecho esto, el cajero efectúa las comprobaciones necesarias con el banco correspondiente, con la comprobación de conformidad del banco, emite las entradas, devuelve la tarjeta al usuario y notifica al ordenador central la venta, éste marca ahora las entradas como vendidas. Si el usuario pulsa la opción de salir, o pasado cierto tiempo no efectúa ninguna otra operación, el cajero pasa automáticamente a la pantalla principal. 

```

